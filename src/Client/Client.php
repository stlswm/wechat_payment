<?php

namespace stlswm\WeChatPayment\Client;


use stlswm\WeChatPayment\Signature\MD5;
use stlswm\WeChatPayment\Signature\ShaHmac256Signature;
use stlswm\WeChatPayment\Signature\SignatureInterface;

/**
 * Class Client
 *
 * @package WeChatPayment
 */
class Client
{
    /**
     * @var string
     */
    public $mchId;
    /**
     * @var string
     */
    public $secret;

    /**
     * @var SignatureInterface
     */
    protected $signature;

    /**
     * Client constructor.
     *
     * @param string $mchId
     * @param string $secret
     */
    public function __construct(string $mchId, string $secret)
    {
        $this->mchId = $mchId;
        $this->secret = $secret;
    }

    /**
     * @param string $type
     *
     * @Author: wm
     * @Date  : 19-2-27
     * @Time  : 上午10:08
     */
    public function setSignature(string $type)
    {
        switch ($type) {
            case 'MD5':
                $this->signature = new MD5();
                break;
            case 'HMAC-SHA256':
                $this->signature = new ShaHmac256Signature();
                break;
            default:
                $this->signature = new MD5();
        }
    }

    /**
     * @return SignatureInterface
     * @Author: wm
     * @Date  : 19-2-27
     * @Time  : 上午10:10
     */
    public function getSignature()
    {
        return $this->signature;
    }
}