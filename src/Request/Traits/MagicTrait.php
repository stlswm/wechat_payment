<?php

namespace stlswm\WeChatPayment\Request\Traits;

use stlswm\WeChatPayment\Request\Request;


/**
 * Trait MagicTrait
 *
 * @package WeChatPayment\Client\Request\Traits
 * @mixin Request
 */
trait MagicTrait
{
    /**
     * @param string $methodName
     * @param int    $start
     *
     * @return string
     */
    protected function propertyNameByMethodName($methodName, $start = 3)
    {
        return \mb_strcut($methodName, $start);
    }
}
