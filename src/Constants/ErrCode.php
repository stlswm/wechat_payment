<?php

/*
|--------------------------------------------------------------------------
| Constants for Cloud
|--------------------------------------------------------------------------
|
| The constants used by SDK are defined here.
| This file will be automatically loaded.
|
*/

/**
 * Credential
 */
define('WECHAT_CLOUD_INVALID_CREDENTIAL', 'SDK.InvalidCredential');
/**
 * Client Not Found
 */
define('WECHAT_CLOUD_CLIENT_NOT_FOUND', 'SDK.ClientNotFound');
/**
 * Server Unreachable
 */
define('WECHAT_CLOUD_SERVER_UNREACHABLE', 'SDK.ServerUnreachable');
/**
 * Invalid RegionId
 */
define('WECHAT_CLOUD_INVALID_REGION_ID', 'SDK.InvalidRegionId');
/**
 * Invalid Argument
 */
define('WECHAT_CLOUD_INVALID_ARGUMENT', 'SDK.InvalidArgument');
/**
 * Service Not Found
 */
define('WECHAT_CLOUD_SERVICE_NOT_FOUND', 'SDK.ServiceNotFound');