<?php

namespace stlswm\WeChatPayment;

use stlswm\WeChatPayment\Traits\RequestTrait;

/**
 * Class WeChatPay
 *
 * @package stlswm\WeChatPayment
 * @mixin RequestTrait
 */
class WeChatPay
{
    use RequestTrait;
    const VERSION = '1.0.1';
}